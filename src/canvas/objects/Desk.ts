import { fabric } from 'fabric';
import { uuid } from 'uuidv4';

import { FabricObject } from '../utils';

export enum DeskType {
    Normal = 'normalDesk',
    Smart = 'smartDesk',
}

export enum DeskStatus {
    Booked = 'booked',
    Available = 'available',
    Disabled = 'disabled',
    Dedicated = 'dedicated',
}

export interface DeskColorConfig {
    fill: string
    border: string
    color?: string
}

export const DeskColors: Record<DeskType | DeskStatus, DeskColorConfig>  = {
	[DeskType.Normal]: {
		fill: '#68A4BD',
		border: '#68A4BD',
	},
	[DeskType.Smart]: {
		fill: '#5AAD73',
		border: '#9B59B6',
	},
	[DeskStatus.Available]: {
		fill: '#5DADE2',
		border: '#3498DB',
	},
	[DeskStatus.Booked]: {
		fill: '#F5B041',
		border: 'rgb(243, 156, 18)',
	},
	[DeskStatus.Disabled]: {
		fill: '#DDDDDD',
		border: '#DDDDDD',
	},
	[DeskStatus.Dedicated]: {
		fill: '#D5E1F4',
		border: '#D5E1F4',
	},
};

export const getEllipsis = (text: string, length: number) => {
	if (!length) {
		return /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/.test(text)
			? text.length > 8
				? text.substring(0, 8).concat('...')
				: text
			: text.length > 15
			? text.substring(0, 15).concat('...')
			: text;
	}
	return /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/.test(text)
		? text.length > length / 2
			? text.substring(0, length / 2).concat('...')
			: text
		: text.length > length
		? text.substring(0, length).concat('...')
		: text;
};

export interface DeskObject extends FabricObject<fabric.Group> {
	errorFlag?: fabric.IText;
	label?: fabric.Text;
	errors?: any;

    status?: DeskStatus;
    metadata?: Record<string, any>;
    deskType?: DeskType;

	setErrors?: (errors: any) => void;
	duplicate?: () => DeskObject;
}

const getDeksColor = (option: DeskObject): DeskColorConfig => {
    let color = DeskColors.normalDesk

    if (option.deskType && DeskColors[option.deskType]) {
        color = DeskColors[option.deskType]
    }

    return color
}

const Desk = fabric.util.createClass(fabric.Group, {
	type: 'desk',
	superType: 'place',
	initialize(options: any) {
		options = options || {};
        this.options = options
        this.isSelected = false;

        const color = getDeksColor(options)

		let name = 'Desk';

		if (options.name) {
			name = getEllipsis(options.name, 18);
		}

		this.label = new fabric.Text(name || 'Desk', {
			fontSize: 16,
			fontFamily: 'polestar',
			fontWeight: 500,
			fill: 'rgba(255, 255, 255, 0.8)',
		});

		const rect = new fabric.Rect({
			rx: 4,
			ry: 4,
			width: 200,
			height: 40,
			fill: color.fill,
			stroke: color.border,
			strokeWidth: 2,
		});

        this.rect = rect

		this.errorFlag = new fabric.IText('\uf071', {
			fontFamily: 'Font Awesome 5 Free',
			fontWeight: 900,
			fontSize: 14,
			fill: 'rgba(255, 0, 0, 0.8)',
			visible: options.errors,
		});

		const deskNode = [this.rect, /* icon, */ this.label, this.errorFlag];

		const option = Object.assign({}, options, {
			id: options.id || uuid(),
			width: 200,
			height: 40,
			originX: 'left',
			originY: 'top',
			hasRotatingPoint: true,
			hasControls: true,
            lockScalingFlip: true,
            lockScalingX: true,
            lockScalingY: true,
            lockSkewingX: true,
            lockSkewingY: true,
		});

		this.callSuper('initialize', deskNode, option);

		this.label.set({
			top: this.label.top + this.label.height / 2 + 4,
			left: this.label.left + 10,
		});

		this.errorFlag.set({
			left: this.rect.left,
			top: this.rect.top,
			visible: options.errors,
		});

        this.setControlsVisibility({
            mt: false, 
            mb: false, 
            ml: false, 
            mr: false, 
            bl: false,
            br: false, 
            tl: false, 
            tr: false,
            mtr: true, 
        });
       
	},
	toObject() {
		return fabric.util.object.extend(this.callSuper('toObject'), {
			id: this.get('id'),
			name: this.get('name'),
			description: this.get('description'),
			superType: this.get('superType'),
			borderColor: this.get('borderColor'),
			borderScaleFactor: this.get('borderScaleFactor'),
			dblclick: this.get('dblclick'),
			deletable: this.get('deletable'),
			cloneable: this.get('cloneable'),
            type: this.get('type'),
            deskType: this.get('deskType'),
            status: this.get('status'),
            metadata: this.get('metadata'),
            options: this.get('options'),
		});
	},
	setErrors(errors: any) {
		this.set({
			errors,
		});
		if (errors) {
			this.errorFlag.set({
				visible: true,
			});
		} else {
			this.errorFlag.set({
				visible: false,
			});
		}
	},
	duplicate() {
		const options = this.toObject();
		options.id = uuid();
		options.name = `${options.name}_clone`;
		return new Desk(options);
	},
    updateSelected(isSelected: boolean) {
        this.isSelected = isSelected;
        const rect: fabric.Rect = this.rect

        if (rect) {
            const color = getDeksColor(this.options)
            rect.set({
                stroke:  this.isSelected ? DeskColors.booked.border : color.border
            })
        }
    },
    toggleReadonlyMode(readonly: boolean) {
        if (readonly) {
            this.selectable = true;
            this.evented = true;
            this.lockMovementX = true;
            this.lockMovementY = true;
            this.hasControls = false;
            this.hasRotatingPoint = false;
        } else {
            this.selectable = true;
            this.evented = true;
            this.lockMovementX = false;
            this.lockMovementY = false;
            this.hasControls = true;
            this.hasRotatingPoint = true;
        }
    },
	_render(ctx: CanvasRenderingContext2D) {
		this.callSuper('_render', ctx);
	},
});

Desk.fromObject = (options: DeskObject, callback: (obj: DeskObject) => any) => {
	return callback(new Desk(options));
};


// @ts-ignore
window.fabric.Desk = Desk;

export default Desk;
